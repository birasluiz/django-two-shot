from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db import models
from django.contrib.auth import logout

# Create your views here.


@login_required
def receipt_list(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)
    context = {
        'receipts': receipts
        }
    return render(request, 'receipts/list.html', context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
        context = {
            'form': form
        }
        return render(request, 'receipts/create.html', context)


@login_required
def category_list(request):
    user = request.user
    categories = ExpenseCategory.objects.filter(owner=user)
    context = {
        'categories': categories
        }
    return render(request, 'receipts/categories.html', context)


@login_required
def account_list(request):
    user = request.user
    accounts = Account.objects.filter(owner=user).annotate(num_receipts=models.Count('receipts'))
    context = {
        'accounts': accounts
    }
    return render(request, 'receipts/accounts.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
        context = {
            'form': form
        }
        return render(request, 'receipts/create_category.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
        context = {
            'form': form
        }
        return render(request, 'receipts/create_account.html', context)


@login_required
def user_logout(request):
    logout(request)
    return redirect('receipts/list.html')
